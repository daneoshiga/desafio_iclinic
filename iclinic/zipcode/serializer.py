# -*- coding: utf-8 -*-

from restless.serializers import JSONSerializer

class BasicSerializer(JSONSerializer):
    def deserialize(self, body):
        """
        Very Hacky deserialize to complain with challenge description
        """
        data = body.decode().split('=')
        return {data[0]: data[1]}

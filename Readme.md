Projeto de Desafio iClinic
==========================

Este repositório contém o projeto desafio da iClinic atualizado da desafio da
[Stored](https://bitbucket.org/daneoshiga/desafio_stored)

O projeto está rodando Django 1.9 no python 3.5, com o banco de dados sqlite, os testes foram
escritos utilizando py.test, com cobertura de teste analisada utilizando coverage.

Todas as requisições feitas estão sendo logadas em um arquivo zipcode.log, que é uma implementação
simples para uma funcionalidade de log

Para rodar esse projeto, é necessário seguir os seguintes passos:

Clonar o repositório
--------------------

    git clone git@bitbucket.org:daneoshiga/desafio_iclinic.git

Criar o virtualenv
------------------

    virtualenv env

    source env/bin/activate

Ou criar o virtualenv com o virtualenvwrapper
---------------------------------------------

    mkvirtualenv env

    workon env

Instalar as dependências
------------------------

    pip install -r requirements.txt

Entrar na pasta do projeto Django
--------------------------------

    cd iclinic

Rodar as migrações no banco de dados
------------------------------------

    ./manage.py migrate

Rodar o servidor de desenvolvimento
-----------------------------------

    ./manage.py runserver

Rodar os testes com relatório de cobertura
------------------------------------------

    py.test --cov=zipcode --cov-report html


Decisões de design do projeto
=============================

```
zipcode
├── apipostmon.py - contém o código que acessa a api postmon
├── api.py - contém todo o código relacionado ao restless
├── models.py - model do projeto
├── serializer.py - Serializer usado no projeto
└── test_zipcode.py - testes criados
zipcode.log - log do sistema
```

O restless espera por padrão JSON como entrada, então para seguir os exemplos
dados no projeto, foi necessário criar um serializer específico, que porém só
funcionará para o caso específico deste projeto.

O endpoint de criação assume que, ao ser chamado novamente, deve atualizar os
dados do banco de dados com os valores da API

O endpoint de exclusão segue a regra de possibilidade de repetição de uma
requisição do REST, logo, não dando erro caso o CEP já não exista.

Próximos passos seriam:

* Usar Mock para a api do postmon nos testes
* Usar algo mais simples que o Django como framework (já que o Django ficou subutilizado nesse caso)